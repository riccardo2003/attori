﻿using System;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;

namespace attori
{
    class Program
    {
        static void Main(string[] args)
        {
            string server = "localhost";
            string user = "user1";
            string psw = "Pippo2003";
            string db = "analisi_programmazione_film";
            string cs = @"server=" + server + ";userid=" + user + ";password=" + psw + ";database=" + db;
            using var con = new MySqlConnection(cs);
            con.Open();

            long i=0;

            DirectoryInfo d = new DirectoryInfo(@"/var/www/html/esercizioAttori/attori"); //Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.csv"); //Getting Text files
            //string str = "";
            foreach (FileInfo file in Files)
            {
                //str = str + ", " + file.Name;
                Console.WriteLine(file.Name);
                //verifico se il file è già stato parsato
                string verifica = "SELECT ID  FROM fileParsati WHERE nomeFile=@nomeFile";
                using var sqlVerifica = new MySqlCommand(verifica, con);
                sqlVerifica.Parameters.AddWithValue("@nomeFile", file.Name);
                sqlVerifica.Prepare();
                try
                {
                    using MySqlDataReader rdr = sqlVerifica.ExecuteReader();
                    if (rdr.Read())
                    {
                        var idRestituito = rdr.GetString(0);
                        Console.WriteLine("FILE PARSATO");
                    }
                    else
                    {                                                                             
                        rdr.Close();
                        if (file.Name != "BancaDati Audiovisivo.csv")
                        {
                            try
                            {
                                TextReader filecsv = new StreamReader(file.Name);
                                var reader = new CsvReader(filecsv, new System.Globalization.CultureInfo("it-it"));
                                var film = reader.GetRecords<Trasmissione>();
                                foreach (Trasmissione trasmissioneAttuale in film)
                                {
                                    //Console.WriteLine(" "+trasmissioneAttuale.Data);

                                    string inserimento = "INSERT INTO Trasmissione (nomerete, data, orainizio, orafine, titoloopera, titolooperaoriginale, titolopuntata, titolopuntataoriginale, numeropuntata, numerostagione, regia) VALUES (@NomeRete, @Data, @OraInizio, @OraFine, @TitoloOpera, @TitoloOperaOriginale, @TitoloPuntata, @TitoloPuntataOriginale, @NumeroPuntata, @NumeroStagione, @Regia)";
                                    using var sqlInserimento = new MySqlCommand(inserimento, con);
                        
                                    sqlInserimento.Parameters.AddWithValue("@NomeRete", trasmissioneAttuale.NomeRete);
                                    sqlInserimento.Parameters.AddWithValue("@Data", trasmissioneAttuale.Data);
                                    sqlInserimento.Parameters.AddWithValue("@OraInizio", trasmissioneAttuale.OraInizio);
                                    sqlInserimento.Parameters.AddWithValue("@OraFine", trasmissioneAttuale.OraFine);
                                    sqlInserimento.Parameters.AddWithValue("@TitoloOpera", trasmissioneAttuale.TitoloOpera);
                                    sqlInserimento.Parameters.AddWithValue("@TitoloOperaOriginale", trasmissioneAttuale.TitoloOperaOriginale);
                                    sqlInserimento.Parameters.AddWithValue("@TitoloPuntata", trasmissioneAttuale.TitoloPuntata);
                                    sqlInserimento.Parameters.AddWithValue("@TitoloPuntataOriginale", trasmissioneAttuale.TitoloPuntataOriginale);
                                    sqlInserimento.Parameters.AddWithValue("@NumeroPuntata", trasmissioneAttuale.NumeroPuntata);
                                    sqlInserimento.Parameters.AddWithValue("@NumeroStagione", trasmissioneAttuale.NumeroStagione);
                                    sqlInserimento.Parameters.AddWithValue("@Regia", trasmissioneAttuale.Regia);

                                    sqlInserimento.Prepare();
                                    try
                                    {
                                        sqlInserimento.ExecuteNonQuery();
                                        i++;
                                        Console.WriteLine("la riga è stata inserita" + file.Name);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("Errore" + e.ToString());
                                    }
                                }
                            }
                            catch
                            {
                                Console.WriteLine("errore nel csv:" + file.Name);
                            }

                            string inserisciFileParsato = "INSERT INTO fileParsati (nomeFile,righeParsate) VALUES (@nomeFile,@righeParsate)";
                            using var sqlFileParsato = new MySqlCommand(inserisciFileParsato, con);
                            sqlFileParsato.Parameters.AddWithValue("@nomeFile", file.Name);
                            sqlFileParsato.Parameters.AddWithValue("@righeParsate", i);
                            sqlFileParsato.Prepare();
                            try
                            {
                                sqlFileParsato.ExecuteNonQuery();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("errore inserimento db" + e.ToString());
                            }
                        }

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Errore" + e.ToString());
                }
            }

            // string csv = "topcrime_20200101_20201231.csv";

        }
    }
    class Trasmissione
    {
        //implement this class
        // public string fileCsv{get;set;}
        public string NomeRete { get; set; }
        public string Data { get; set; }
        public string OraInizio { get; set; }
        public string OraFine { get; set; }
        public string TitoloOpera { get; set; }
        public string TitoloOperaOriginale { get; set; }
        public string TitoloPuntata { get; set; }
        public string TitoloPuntataOriginale { get; set; }
        public string NumeroPuntata { get; set; }
        public string NumeroStagione { get; set; }
        public string Regia { get; set; }

    }

    class BancaDati
    {
        public string titoloItaliano{get;set;}
        public string titoloOriginale{get;set;}
        public string numeroStagione{get;set;}
        public string titoloPuntata{get;set;}
        public string numeroPuntata{get;set;}
        public string artistaRasiPrimari{get;set;}
        public string artistaRasiComprimari{get;set;}
        public string artistaRasiDoppiatoriPrimari{get;set;}
        public string artistaRasiDoppiatoriComprimari{get;set;}
        public string regia{get;set;}
        public string annoProduzione{get;set;}
        public string annoPubblicazione{get;set;}
        public string tipologiaOpera{get;set;}
        public string tipoTrasmissione{get;set;}
        public string paeseProduzione{get;set;}
    }

}